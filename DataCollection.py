import spidev
import RPi.GPIO as GPIO
import array as arr
import numpy as np
import time
import os
import threading
import psutil
import matplotlib.pyplot as plt

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)

Reset_pin = 22# Reset_pin = GPIO22
Start_pin = 23# Start_pin = GPIO23
DRDY_pin = 24# DRDY_pin = GPIO 24

LED = 17# LED pin for tests

GPIO.setup(Reset_pin, GPIO.OUT)
GPIO.setup(Start_pin, GPIO.OUT)
GPIO.setup(DRDY_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(LED, GPIO.OUT)
GPIO.output(Reset_pin, True) #Reset High at the begining
GPIO.output(Start_pin, False) #pull down Start to control ADS by code

# version_button = 26
# start_button = 19
# stop_button = 13
# GPIO.setup(version_button, GPIO.IN, pull_up_down=GPIO.PUD_DOWN) #key
# GPIO.setup(start_button, GPIO.IN, pull_up_down=GPIO.PUD_DOWN) #key
# GPIO.setup(stop_button, GPIO.IN, pull_up_down=GPIO.PUD_DOWN) #key



WAKEUP = 0X02
STANDBY = 0X04
RESET = 0X06
START = 0X08
STOP = 0X0A
RDATAC = 0X10
SDATAC = 0X11
RDATA = 0X12
RREG = 0x20
WREG = 0x40
ID = 0x00
CONFIG1 = 0x01
CONFIG2 = 0x02
CONFIG3 = 0x03
LOFF = 0x04
CHnSET = 0x04
CH1SET = 0x05
CH2SET = 0x06
CH3SET = 0x07
CH4SET = 0x08
CH5SET = 0x09
CH6SET = 0x0A
CH7SET = 0x0B
CH8SET = 0x0C
RLD_SENSP = 0x0d
RLD_SENSN = 0x0e
LOFF_SENSP = 0x0f
LOFF_SENSN = 0x10
LOFF_FLIP = 0x11
LOFF_STATP = 0x12
LOFF_STATN = 0x13
PACE = 0x15
RESP = 0x16
CONFIG4 = 0x17
WCT1 = 0x18
WCT2 = 0x19
RLD_SENSP = 0x0D
RLD_SENSN = 0x0E

ADS1298 = spidev.SpiDev()
ADS1298.open(0 , 0) 
ADS1298.max_speed_hz = 300000 #16000000 (for) #380000 
ADS1298.mode = 1 
time.sleep(0.3)

def WREG (address, value):
	opcode1 = address + 0x40
	opcode2 = 0x00
	opcode3 = value
	ADS1298.xfer([opcode1, opcode2, opcode3])
	time.sleep(0.1)
	
def RREG (address):
	opcode1 = address + 0x20
	opcode2 = 0x00
	opcode3 = 0x00
	print(ADS1298.xfer([opcode1, opcode2, opcode3]))
def GetDeviceID():
	RREG(0)
	time.sleep(0.1)


def RREGS (address, num_reg):
	for i in range (num_reg):
		opcode1 = address + 0x20
		opcode2 = 0x00
		opcode3 = 0x00
		tmp = ADS1298.xfer([opcode1, opcode2, opcode3])
		print("Register",address, "=",tmp)
		address = address + 0x01
		time.sleep(0.1)
		
		
def ADS_START():
	ADS1298.writebytes([START])
	#time.sleep(0.01)
def ADS_RDATAC():
	ADS1298.writebytes([RDATAC])
	#time.sleep(0.01)
def ADS_STOP():
	ADS1298.writebytes([STOP])
def ADS_RESET():
	ADS1298.writebytes([RESET])
	time.sleep(0.1)

def ADS_SDATAC():
	ADS1298.writebytes([SDATAC])
	time.sleep(0.1)
def ADS_WAKEUP():
	ADS1298.writebytes([WAKEUP])
	
	
def ADS_Init ():
	ADS_RESET()
	ADS_SDATAC()
	#Device_ID = GetDeviceID(); 
	time.sleep(0.3)
	
	WREG(CONFIG1, 0x86)
	time.sleep(0.1)
	WREG(CONFIG2, 0x02) #0x02 x010
	time.sleep(0.1)
	WREG(CONFIG3, 0xEC)
	time.sleep(0.1)
	#WREG(CONFIG4, 0x02)
	
	WREG(LOFF, 0x12)
	
	
	WREG(CH1SET, 0x81)
	time.sleep(0.01)
	WREG(CH2SET, 0x60)
	time.sleep(0.01)
	WREG(CH3SET, 0x60)
	time.sleep(0.01)
	WREG(CH4SET, 0x60)
	time.sleep(0.01)
	WREG(CH5SET, 0x60)
	time.sleep(0.01)
	WREG(CH6SET, 0x60)
	time.sleep(0.01)
	WREG(CH7SET, 0x81)
	time.sleep(0.01)
	WREG(CH8SET, 0x81)
	time.sleep(0.01)
	
	WREG(RLD_SENSP, 0x3E)
	time.sleep(0.01)
	
	WREG(RLD_SENSN, 0x3E)
	time.sleep(0.01)
	
	#time.sleep(0.1)
	
	#RREGS(0, 17)
	#time.sleep(0.1)
	
	ADS_START()
	ADS_RDATAC()
def BytesToHex(Bytes):
	return ''.join(["0x%02X " % x for x in Bytes]).strip()


x,y = [], []
ch1,ch2,ch3,ch4,ch5 = [],[],[],[],[]
ch11,ch22,ch33,ch44,ch55 = [],[],[],[],[]
mvscale = 39.74 * 1e-6

count =0
condition = 0

ADS_Init()

def my_callback(channel):
	global count, condition
	temp_data = ADS1298.xfer([0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00])
	y.append(temp_data)
	
	#time.sleep(0.1)
	# for i in range (27):
		# y.append(ADS1298.xfer([0x00]))

	count = count + 1
	if (count>=4000):
		#ADS_STOP()
		#time.sleep(0.3)
		
		ADS_RESET()
		time.sleep(0.1)
		ADS_SDATAC()
		time.sleep(0.1)
		#ADS_STOP()
		
		ADS1298.close()
		#time.sleep(0.1)
		#GPIO.cleanup()
		condition = 1

GPIO.add_event_detect(DRDY_pin, GPIO.FALLING, callback = my_callback) #RISING/FALLING bouncetime=10


while (True):
	time.sleep(0)
	if (condition == 1):
		for i in range(len(y)):
			if (len(y[i]) != 27):
				print ("This set of data is not 27 bytes!!", i)
				print("length of damaged data pack = ", len(y[i]))
		#print(y)
		for i in range (count):
			data = y[i]
			
			ch1.append(data[6])
			ch1.append(data[7])
			ch1.append(data[8])
			
			ch2.append(data[9])
			ch2.append(data[10])
			ch2.append(data[11])
			
			ch3.append(data[12])
			ch3.append(data[13])
			ch3.append(data[14])
			
			ch4.append(data[15])
			ch4.append(data[16])
			ch4.append(data[17])
			
			ch5.append(data[18])
			ch5.append(data[19])
			ch5.append(data[20])
		
		
		for p in range ((len(ch1)//3)-1):
			target = p*3
			
			#buf_data = (ch1[target]*1000000) + (ch1[target+1]*1000) + (ch1[target+2])
			buf_data = (ch1[target]<<16) + (ch1[target+1]<<8) + (ch1[target+2])
			
			if buf_data > 8388607:
				buf_data = buf_data - 16777216
			buf_data = buf_data * mvscale
			ch11.append(buf_data)
			
			#buf_data = (ch2[target]*1000000) + (ch2[target+1]*1000) + (ch2[target+2])
			buf_data = (ch2[target]<<16) + (ch2[target+1]<<8) + (ch2[target+2])
			if buf_data > 8388607:
				buf_data = buf_data - 16777216
			buf_data = buf_data * mvscale
			ch22.append(buf_data)
			
			#buf_data = (ch3[target]*1000000) + (ch3[target+1]*1000) + (ch3[target+2])
			buf_data = (ch3[target]<<16) + (ch3[target+1]<<8) + (ch3[target+2])
			if buf_data > 8388607:
				buf_data = buf_data - 16777216
			buf_data = buf_data * mvscale
			ch33.append(buf_data)
			
			#buf_data = (ch4[target]*1000000) + (ch4[target+1]*1000) + (ch4[target+2])
			buf_data = (ch4[target]<<16) + (ch4[target+1]<<8) + (ch4[target+2])
			if buf_data > 8388607:
				buf_data = buf_data - 16777216
			buf_data = buf_data * mvscale
			ch44.append(buf_data)
			
			#buf_data = (ch5[target]*1000000) + (ch5[target+1]*1000) + (ch5[target+2])
			buf_data = (ch5[target]<<16) + (ch5[target+1]<<8) + (ch5[target+2])
			if buf_data > 8388607:
				buf_data = buf_data - 16777216
			buf_data = buf_data * mvscale
			ch55.append(buf_data)
		
		
		
		# print("y = ", y)
		# print("ch1 = \n", ch11)
			
		for j in range (len(ch11)):
			x.append(j)
			
		#plot channels
		plt.figure()
	
		plt.subplot(321)
		plt.plot(x,ch11)
		plt.title('CH1', fontsize = 20, fontweight = "bold")
	
		plt.subplot(322)
		plt.plot(x,ch22)
		plt.title('CH2', fontsize = 20, fontweight = "bold")
	
		plt.subplot(323)
		plt.plot(x,ch33)
		plt.title('CH3', fontsize = 20, fontweight = "bold")
	
		plt.subplot(324)
		plt.plot(x,ch44)
		plt.title('CH4', fontsize = 20, fontweight = "bold")
	
		plt.subplot(325)
		plt.plot(x,ch55)
		plt.title('CH5', fontsize = 20, fontweight = "bold")
		plt.ylabel('(Arbitrary Units)', fontweight = "bold")
		plt.xlabel('(Sample)', fontweight = "bold")
		
		condition = 0
		plt.show()
		
		# for k in range (len(y)//27):
			# target = k*27
			
			# temp1 = y[target + 6]
			# temp2 = y[target + 7]
			# temp3 = y[target + 8]
			# ch1.append((temp1 [0]*1000000) + (temp2 [0]*1000) + (temp3 [0]))
			
			# temp1 = y[target + 9]
			# temp2 = y[target + 10]
			# temp3 = y[target + 11]
			# ch2.append((temp1 [0]*1000000) + (temp2 [0]*1000) + (temp3 [0]))

			# temp1 = y[target + 12]
			# temp2 = y[target + 13]
			# temp3 = y[target + 14]
			# ch3.append((temp1 [0]*1000000) + (temp2 [0]*1000) + (temp3 [0]))

			# temp1 = y[target + 15]
			# temp2 = y[target + 16]
			# temp3 = y[target + 17]
			# ch4.append((temp1 [0]*1000000) + (temp2 [0]*1000) + (temp3 [0]))
			
			# temp1 = y[target + 18]
			# temp2 = y[target + 19]
			# temp3 = y[target + 20]
			# ch5.append((temp1 [0]*1000000) + (temp2 [0]*1000) + (temp3 [0]))
			
		# print(len(ch1))
		# for j in range (len(ch1)):
			# x.append(j)
			
		# #plot channels
		# plt.figure()
	
		# plt.subplot(511)
		# plt.plot(x,ch1)
		# plt.title('CH1')
	
		# plt.subplot(512)
		# plt.plot(x,ch2)
		# plt.title('CH2')
	
		# plt.subplot(513)
		# plt.plot(x,ch3)
		# plt.title('CH3')
	
		# plt.subplot(514)
		# plt.plot(x,ch4)
		# plt.title('CH4')
	
		# plt.subplot(515)
		# plt.plot(x,ch5)
		# plt.title('CH5')

		# condition = 0
		# plt.show()
		exit()


